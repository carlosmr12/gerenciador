# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from models import ItemAgenda

from django.template import RequestContext
from forms import FormItemAgenda


def lista(request):
    lista_itens = ItemAgenda.objects.all()
    return render_to_response("lista.html", {'lista_itens': lista_itens})
    
def adiciona(request):
    if request.method == 'POST': #Form sent
        form = FormItemAgenda(request.POST, request.FILES)
        if form.is_valid():
            
            #form validated
            dados = form.cleaned_data
            item = ItemAgenda(data=dados['data'],
                hora=dados['hora'],
                titulo=dados['titulo'],
                descricao=dados['descricao'])
            item.save()

            # Feedback message
            return render_to_response("salvo.html", {})

        else: # page was accessed by GET method
            # Show blank form
            form = FormItemAgenda()

        return render_to_response("adiciona.html", {'form':form}, context_instance=RequestContext(request))
